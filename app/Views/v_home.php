<h3>Nagari Sarilamak</h3>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- Small boxes (Stat box) -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Datamaster">Jumlah Penduduk</a></span>
                        <span class="info-box-number">
                            <?= $jml_penduduk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-male"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_lk">Jumlah Laki Laki</a></span>
                        <span class="info-box-number">
                            <?= $jml_lk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-female"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_pr">Jumlah perempuan</a></span>
                        <span class="info-box-number">
                            <?= $jml_pr; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Datakk">Jumlah Keluarga</a></span>
                        <span class="info-box-number">
                            <?= $jml_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
        </div>
    </div>
</section>
<br>
<h3>Jumlah Keluarga Perjorong</h3>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- Small boxes (Stat box) -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_kk_sarilamak">Jorong Sarilamak</a></span>
                        <span class="info-box-number">
                            <?= $jml_sarilamak_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_kk_purwajaya">Jorong Purwajaya</a></span>
                        <span class="info-box-number">
                            <?= $jml_purwajaya_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_kk_ketinggian">Jorong Ketinggian</a></span>
                        <span class="info-box-number">
                            <?= $jml_ketinggian_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_kk_air_putih">Jorong Air Putih</a></span>
                        <span class="info-box-number">
                            <?= $jml_air_putih_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_kk_buluh_kasok">Jorong Buluh Kasok</a></span>
                        <span class="info-box-number">
                            <?= $jml_buluh_kasok_kk; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
        </div>
    </div>
</section>
<br>
<h3>Jumlah Penduduk Perjorong</h3>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- Small boxes (Stat box) -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_sarilamak">Jorong Sarilamak</a></span>
                        <span class="info-box-number">
                            <?= $jml_sarilamak; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_purwajaya">Jorong Purwajaya</a></span>
                        <span class="info-box-number">
                            <?= $jml_purwajaya; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_ketinggian">Jorong Ketinggian</a></span>
                        <span class="info-box-number">
                            <?= $jml_ketinggian; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_air_putih">Jorong Air Putih</a></span>
                        <span class="info-box-number">
                            <?= $jml_air_putih; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_buluh_kasok">Jorong Buluh Kasok</a></span>
                        <span class="info-box-number">
                            <?= $jml_buluh_kasok; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
        </div>
    </div>
</section>
<br>
<h3>Jumlah Agama</h3>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- Small boxes (Stat box) -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_sarilamak">ISLAM</a></span>
                        <span class="info-box-number">
                            <?= $jml_sarilamak; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_purwajaya">PROTESTAN</a></span>
                        <span class="info-box-number">
                            <?= $jml_purwajaya; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_ketinggian">KATOLIK</a></span>
                        <span class="info-box-number">
                            <?= $jml_ketinggian; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_air_putih">HINDU</a></span>
                        <span class="info-box-number">
                            <?= $jml_air_putih; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_buluh_kasok">BUDDHA</a></span>
                        <span class="info-box-number">
                            <?= $jml_buluh_kasok; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a type="button" href="Home/jml_buluh_kasok">KONG HU CU</a></span>
                        <span class="info-box-number">
                            <?= $jml_buluh_kasok; ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
        </div>
    </div>
</section>